﻿using System;
using System.Collections.Generic;

namespace Colecciones
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> numeros = new List<int>(); //Declarar una coleccion de tipo lista

            //Console.WriteLine("Cuantos elementos quieres introducir?");

            //int elem = Int32.Parse(Console.ReadLine());

            //for(int i=0;i<elem;i++)
            //{
            //    Console.WriteLine("Numero " + (1 + i));
            //    numeros.Add(Int32.Parse(Console.ReadLine()));
            //}
            //for(int i=0;i<numeros.Count;i++)
            //{
            //    Console.WriteLine("\n" + numeros[i]);
            //}

            Console.WriteLine("Introduce elementos en la coleccion (0 para salir)");
            int elem = 1;
            while (elem != 0)
            {
                elem = Int32.Parse(Console.ReadLine());
                numeros.Add(elem);
            }

            numeros.RemoveAt(numeros.Count - 1);
            Console.WriteLine("Elementos introducidos: ");

            foreach (int numero in numeros)
            {
                Console.WriteLine(numero);
            }
        }
    }
}
