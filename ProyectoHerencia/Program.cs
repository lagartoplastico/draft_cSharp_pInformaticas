﻿using System;

namespace ProyectoHerencia
{
    class Program
    {
        static void Main(string[] args)
        {
            //Caballo Babieca = new Caballo("Babieca Eca");
            //ISaltoConPatas ImiBabieca = Babieca;
            //Humano Javier = new Humano("Javier Niche");
            //Gorila Copito = new Gorila("Copito de Nieve");


            ////Object miAnimal = new Caballo();
            ////Object mipersona = new Humano();
            ////Object mimamifero = new Mamiferos();

            ////Mamiferos[] almacenAnimales = new Mamiferos[3];
            ////almacenAnimales[0] = Babieca;
            ////almacenAnimales[1] = Javier;
            ////almacenAnimales[2] = Copito;
            ////for(int i=0;i<3;i++)
            ////{
            ////    almacenAnimales[i].pensar();
            ////    Console.WriteLine("\n");
            ////}

            //Ballena miWally = new Ballena("Wally");
            //miWally.nadar();

            //Console.WriteLine("Patas utilizadas en el salto de Babieca: " + ImiBabieca.numeroPatas());

            Lagartija Juancho = new Lagartija("Juancho Dibujitos");
            Juancho.getNombre();
            Humano Nelson = new Humano("Nelson Ned");
            Nelson.getNombre();
        }
    }

    interface IMamiferosTerrestres
    {
        int numeroPatas();

    }
    interface IAnimalesYDeportes
    {
        string tipoDeporte();
        bool esOlimpico();
    }
    interface ISaltoConPatas
    {
        int numeroPatas();
    }
    abstract class Animales
    {
        public void respirar()
        {
            Console.WriteLine("Soy capaz de respirar");
        }
        public abstract void getNombre();

    }

    class Lagartija : Animales
    {
        public Lagartija(string nombre)
        {
            nombreReptil = nombre;
        }
        public override void getNombre()
        {
            Console.WriteLine("El nombre del Reptil es:     " + nombreReptil);
        }

        private string nombreReptil;
    }
    class Mamiferos : Animales
    {
        public Mamiferos(string nombre)
        {
            nombreSerVivo = nombre;
        }

        public virtual void pensar()
        {
            Console.WriteLine("Pensamiento Basico Instintivo");
        }
        public void cuidarCrias()
        {
            Console.WriteLine("Cuido de mis crias hasta que se valgan por si solas");
        }
        public override void getNombre()
        {
            Console.WriteLine("El nombre del Mamifero es:   " + nombreSerVivo);
        }

        private string nombreSerVivo;
    }

    class Ballena : Mamiferos
    {
        public Ballena(string nombreBallena) : base(nombreBallena)
        {

        }
        public void nadar()
        {
            Console.WriteLine("Soy Capaz de nadar");
        }
    }
    class Caballo : Mamiferos, IMamiferosTerrestres, IAnimalesYDeportes, ISaltoConPatas
    {
        public Caballo(string nombreCaballo) : base(nombreCaballo)
        {

        }

        public void galopar()
        {
            Console.WriteLine("Soy capaz de galopar");
        }
        int IMamiferosTerrestres.numeroPatas()
        {
            return 4;
        }
        int ISaltoConPatas.numeroPatas()
        {
            return 2;
        }
        public string tipoDeporte()
        {
            return "Hipica";
        }
        public bool esOlimpico()
        {
            return true;
        }
    }
    class Humano : Mamiferos
    {
        public Humano(string nombreHumano) : base(nombreHumano)
        {

        }
        public override void pensar()
        {
            Console.WriteLine("Soy capaz de pensar ???");
        }
    }

    sealed class Gorila : Mamiferos, IMamiferosTerrestres
    {
        public Gorila(string nombreGorila) : base(nombreGorila)
        {

        }
        public void trepar()
        {
            Console.WriteLine("Soy capaz de trepar");
        }
        public override void pensar()
        {
            Console.WriteLine("Pensamiento Instintivo Avanzado");
        }
        public int numeroPatas()
        {
            return 2;
        }
    }

}
