﻿using System;

namespace GenericosRestricciones
{
    class Program
    {
        static void Main(string[] args)
        {
            AlmacenEmpleados<Electricista> empleados = new AlmacenEmpleados<Electricista>(3);

            empleados.Agregar(new Electricista(10500));
            empleados.Agregar(new Electricista(20500));
            empleados.Agregar(new Electricista(40500));
        }
    }

    class AlmacenEmpleados<T> where T : IParaEmpleados
    {
        public AlmacenEmpleados(int z)
        {
            datosEmpleado = new T[z];
        }
        public void Agregar(T obj)
        {
            datosEmpleado[i] = obj;
            i++;
        }
        public T getEmpleado(int i)
        {
            return datosEmpleado[i];
        }

        private T[] datosEmpleado;
        private int i = 0;
    }
    interface IParaEmpleados
    {
        double GetSalario();
    }
    class Director : IParaEmpleados
    {
        public Director(double salario)
        {
            this.salario = salario;
        }
        public double GetSalario()
        {
            return salario;
        }
        private double salario;
    }
    class Secretaria : IParaEmpleados
    {
        public Secretaria(double salario)
        {
            this.salario = salario;
        }
        public double GetSalario()
        {
            return salario;
        }
        private double salario;

    }
    class Electricista : IParaEmpleados
    {
        public Electricista(double salario)
        {
            this.salario = salario;
        }
        public double GetSalario()
        {
            return salario;
        }
        private double salario;
    }
    class Estudiante
    {
        public Estudiante(double edad)
        {
            this.edad = edad;
        }
        public double GetEdad()
        {
            return edad;
        }
        private double edad;
    }
}
