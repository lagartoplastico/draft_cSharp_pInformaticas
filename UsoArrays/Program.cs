﻿using System;

namespace UsoArrays
{
    class Program
    {
        static void Main(string[] args)
        {

            //int[] edades;
            //edades = new int[4];
            //edades[0] = 15;
            //edades[1] = 27;
            //edades[2] = 19;
            //edades[3] = 80;

            //int[] edades = { 100, 300, 560, 8888 };
            //Console.WriteLine(edades[2]);

            var valores = new[] { 1, 2, 3, 4, 111111.1, 543 };


            Empleados Ana = new Empleados("Ana", 27);
            Empleados[] arrayEmpleados = new Empleados[3];
            arrayEmpleados[0] = new Empleados("Sara", 76);
            arrayEmpleados[1] = Ana;
            arrayEmpleados[2] = new Empleados("Manuel", 51);

            //clases anonimas

            var personas = new[]
            {
                new{Nombre="Juan",Edad=19},
                new{Nombre="Maria",Edad=49},
                new{Nombre="Dayana",Edad=35}
            };

            //Console.WriteLine(personas[1]);

            //for (int i = 0; i < arrayEmpleados.Length; i++)
            //{
            //    Console.WriteLine(arrayEmpleados[i].getInfo());
            //}

            //foreach(Empleados empleado in arrayEmpleados)
            //{
            //    Console.WriteLine(empleado.getInfo());
            //}

            //foreach(double valor in valores)
            //{
            //    Console.WriteLine(valor);
            //}

            foreach (var persona in personas)
            {
                Console.WriteLine(persona);
            }

        }
    }
    class Empleados
    {
        public Empleados(string nombre, int edad)
        {

            this.nombre = nombre;
            this.edad = edad;

        }
        public string getInfo()
        {
            return "Nombre: " + nombre + "   Edad: " + edad + "\n";
        }
        private string nombre;
        private int edad;
    }
}
