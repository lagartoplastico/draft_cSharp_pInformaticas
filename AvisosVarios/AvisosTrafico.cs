﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AvisosVarios
{
    class AvisosTrafico : IAvisos
    {
        public AvisosTrafico()
        {
            remitente = "Direccion General de Transisto";
            mensaje = "Sansion cometida. Pague antes de 3 dias y se beneficiara de una reduccion del 50%";
            fecha = "";
        }
        public AvisosTrafico(string remitente, string mensaje, string fecha)
        {
            this.remitente = remitente;
            this.mensaje = mensaje;
            this.fecha = fecha;
        }
        public string getFecha()
        {
            return fecha;
        }

        public void mostrarAviso()
        {
            Console.WriteLine($"Mensaje: {mensaje}\n    Remitente: {remitente}\n    Fecha: {fecha}\n");
        }

        private string remitente;
        private string mensaje;
        private string fecha;
    }
}
