﻿using System;

namespace AvisosVarios
{
    class Program
    {
        static void Main(string[] args)
        {
            AvisosTrafico av1 = new AvisosTrafico();
            av1.mostrarAviso();

            AvisosTrafico av2 = new AvisosTrafico("Jefatura de Policia de Transito", "Exceso de velocidad $500", "02/12/2019");
            Console.WriteLine("--------------------------------\nFecha solicitada" + av2.getFecha() + "\n--------------------------------\n");
            av2.mostrarAviso();
        }
    }
}
