﻿using System;

namespace genericos
{
    class Program
    {
        static void Main(string[] args)
        {
            AlmacenObjetos<DateTime> archivos = new AlmacenObjetos<DateTime>(4);


            //archivos.Agregar("Verito");
            //archivos.Agregar("Rex");
            //archivos.Agregar("aylin");
            //archivos.Agregar("joan");

            archivos.Agregar(new DateTime()); //0
            archivos.Agregar(new DateTime()); //1
            archivos.Agregar(new DateTime()); //2
            archivos.Agregar(new DateTime()); //3


            DateTime nombrePersona = archivos.GetElement(2);
            Console.WriteLine(nombrePersona);


        }
    }

    class AlmacenObjetos<MITIPO>
    {
        public AlmacenObjetos(int z)
        {
            datosElemento = new MITIPO[z];
        }
        public void Agregar(MITIPO obj)
        {
            datosElemento[i] = obj;
            i++;
        }
        public MITIPO GetElement(int i)
        {
            return datosElemento[i];
        }


        private MITIPO[] datosElemento;
        private int i = 0;
    }
    class Empleado
    {
        public Empleado(double salario)
        {
            this.salario = salario;
        }
        public double GetSalario()
        {
            return salario;
        }

        private double salario;
    }
}
