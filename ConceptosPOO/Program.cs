﻿using System;


namespace ConceptosPOO
{
    class Program
    {
        static void Main(string[] args)
        {
            // realizarTarea();
            var miVariable = new { Nombre = "Joseph", Edad = 19 };
            Console.WriteLine(miVariable.Nombre + " " + miVariable.Edad);
            var miOtraVariable = new { Nombre = "Ana", Edad = 88 };
            miVariable = miOtraVariable;
        }

        static void realizarTarea()
        {

            Punto origen = new Punto();
            Punto destino = new Punto(128, 80);
            Punto nuevoPunto = new Punto(11, 500);

            double distancia = origen.distanciaHasta(destino);
            double distancia2 = destino.distanciaHasta(nuevoPunto);

            Console.WriteLine($"Distancia entre los puntos es {distancia}\n Segunda distancia calculada: {distancia2}");
            Console.WriteLine($"Numbero de objetos creados: {Punto.ContadorDeObjetos()}");
        }
    }

}
