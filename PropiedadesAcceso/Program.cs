﻿using System;

namespace PropiedadesAcceso
{
    class Program
    {
        static void Main(string[] args)
        {
            Empleado Juan = new Empleado("Juan Travolta");
            Juan.SALARIO = -1200;

            Console.WriteLine("**** El salario del empleado es:  " + Juan.SALARIO +" ****");
        }
    }
    class Empleado
    {
        public Empleado(string nombre)
        {
            this.nombre = nombre;
        }
        //public void setsalario(double salario)
        //{
        //    if (salario < 0)
        //    {
        //        Console.WriteLine("El salario no puede ser negativo. Salario asignado = 0");
        //        this.salario = 0;
        //    }
        //    else
        //        this.salario = salario;

        //}
        //public double getSalario()
        //{
        //    return salario;
        //}


        private double evaluaSalario(double salario)
        {
            if (salario < 0) return 0;
            else return salario;

        }

        // CREACION DE PROPIEDAD

        //public double SALARIO
        //{
        //    get { return this.salario; }
        //    set { this.salario = evaluaSalario(value); }
        //}

        // CREACION DE PROPIEDAD CON LAMBDA
        public double SALARIO
        {
            get => this.salario;
            set => this.salario = evaluaSalario(value);
        }
        private string nombre;
        private double salario;
    }
}
