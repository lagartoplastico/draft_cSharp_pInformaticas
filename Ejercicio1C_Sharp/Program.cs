﻿using System;

namespace Ejercicio1C_Sharp
{
    class Program
    {
        static void Main(string[] args)
        {
            Coche Wolkswagen = new Coche(true, "rojo", false);
            Coche Toyota = new Coche(false, "blanco", true);
            Avion Boing = new Avion(false, "naranja", false);
            Avion NaveMilitar = new Avion(true, "camuflado", true);
            Motocicleta Honda = new Motocicleta(true, "negro");
            Motocicleta Susuki = new Motocicleta(false, "azul");
            Vehiculo TanqueRuso = new Coche(true, "Caqui", true);

            //Wolkswagen.arrancarMotor();
            //Toyota.arrancarMotor();
            //Wolkswagen.pararMotor();
            //Toyota.pararMotor();
            //Toyota.cuatroPorCuatro();

            //Boing.arrancarMotor();
            //NaveMilitar.pararMotor();
            //NaveMilitar.aterrizajeEnAgua();

            //Honda.getColor();
            //Susuki.SaltoCross();

            TanqueRuso.conducir();
            Wolkswagen.conducir("Miguel Santos");
            Boing.conducir("Carlos Marquez");
            Honda.conducir();


        }
    }
}
