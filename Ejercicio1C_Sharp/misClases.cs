﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio1C_Sharp
{
    class misClases
    {
    }
    class Vehiculo
    {
        public Vehiculo(bool estado, string color)
        {
            estadoMotor = estado;
            this.color = color;

        }
        public void arrancarMotor()
        {
            if (!estadoMotor)
            {
                estadoMotor = true;
                Console.WriteLine("Arranque iniciado");

            }
            else
            {
                Console.WriteLine("El motor ya se encuentra arrancado");
            }
        }
        public void pararMotor()
        {
            if (estadoMotor)
            {
                estadoMotor = false;
                Console.WriteLine("Se detuvo el arranque");

            }
            else
            {
                Console.WriteLine("El motor ya se encuentra detenido");
            }
        }
        public virtual void conducir()
        {
            Console.WriteLine("Este vehiculo puede conducirse");
        }
        public void getColor()
        {
            Console.WriteLine("El vehiculo es de color " + color);
        }

        private bool estadoMotor;
        protected string color;

    }
    class Coche : Vehiculo
    {
        public Coche(bool estadoCoche, string colorCoche, bool conTraccion4x4) : base(estadoCoche, colorCoche)
        {
            this.conTraccion4x4 = conTraccion4x4;
        }
        public void conducir(string conductor)
        {
            Console.WriteLine($"El coche {color} es conducido por {conductor}");
        }
        public void cuatroPorCuatro()
        {
            if (conTraccion4x4)
                Console.WriteLine("El coche tiene traccion 4 x 4 ");
            else
                Console.WriteLine("El coche no cuenta con traccion en las cuatro ruedas");
        }

        private bool conTraccion4x4;

    }
    class Avion : Vehiculo
    {
        public Avion(bool estadoAvion, string colorAvion, bool aterrizaEnAgua) : base(estadoAvion, colorAvion)
        {
            this.aterrizaEnAgua = aterrizaEnAgua;
        }
        public void conducir(string piloto)
        {
            Console.WriteLine($"El avion {color} es piloteado por {piloto}");
        }
        public void aterrizajeEnAgua()
        {
            if (aterrizaEnAgua)
                Console.WriteLine("El avion puede realizar aterrizaje en agua");
            else
                Console.WriteLine("El avion no es apto para aterrizaje sobre agua");
        }

        private bool aterrizaEnAgua;
    }
    class Motocicleta : Vehiculo
    {
        public Motocicleta(bool estadoMoto, string colorMoto) : base(estadoMoto, colorMoto)
        {

        }
        public void SaltoCross()
        {
            Console.WriteLine("La moto realizo un salto de motocross.* * * Medidas de seguridad tomadas * * *");
        }

    }
}
