﻿using System;
using System.Collections.Generic;

namespace DelegadoPredicado2
{
    class Program
    {
        static void Main(string[] args)
        {
            //List<Personas> gente = new List<Personas>();

            //Personas P1 = new Personas();
            //P1.Nombre = "Juan";
            //P1.Edad = 18;

            //Personas P2 = new Personas();
            //P2.Nombre = "Maria";
            //P2.Edad = 28;

            //Personas P3 = new Personas();
            //P3.Nombre = "Ana";
            //P3.Edad = 37;

            //gente.AddRange(new Personas[] { P1, P2, P3 });

            //Predicate<Personas> elPredicado = new Predicate<Personas>(ExisteJuan);

            //bool existe = gente.Exists(elPredicado);

            //if (existe) Console.WriteLine("Existen presonas llamadas Juan");
            //else Console.WriteLine("No hay personas llamadas Juan");



            //OperacionesMatematicas operacion = new OperacionesMatematicas(Cuadrado);
            OperacionesMatematicas operacion = new OperacionesMatematicas(num => num * num);

            Console.WriteLine(operacion(4));

            //OperacionesMatematicas1 operacion1 = new OperacionesMatematicas1(Suma);
            OperacionesMatematicas1 operacion1 = new OperacionesMatematicas1((nume1, nume2) => nume1 + nume2);

            Console.WriteLine(operacion1(5, 10));

        }

        //static bool ExisteJuan(Personas persona)
        //{
        //    if (persona.Nombre == "Juan") return true;
        //    else return false;
        //}

        public delegate int OperacionesMatematicas(int numero);
        public delegate int OperacionesMatematicas1(int numero1, int numero2);

        //public static int Cuadrado(int num)
        //{
        //    return num * num;
        //}

        //public static int Suma(int num1,int num2)
        //{
        //    return num1 + num2;
        //}
    }

    class Personas
    {
        private string nombre;
        private int edad;

        public string Nombre { get => nombre; set => nombre = value; }
        public int Edad { get => edad; set => edad = value; }
    }
}
