﻿using System;

namespace UsoArrays2
{
    class Program
    {
        static void Main(string[] args)
        {
            //int[] numeros = new int[4] { 7, 9, 15, 3 };

            //ProcesaDatos(numeros);

            //foreach (int n in numeros)
            //{
            //    Console.WriteLine(n);
            //}

            int[] arrayElementos = LeerDatos();
            Console.WriteLine("----- Imprimiendo desde la clase MAIN -----");
            foreach (int e in arrayElementos)
            {
                Console.WriteLine(" - " + e + " - ");
            }

        }

        static int[] LeerDatos()
        {
            Console.WriteLine("Cuantos elementos?");
            string respuesta = Console.ReadLine();
            int numElementos = int.Parse(respuesta);
            int[] datos = new int[numElementos];
            for (int i = 0; i < numElementos; i++)
            {
                Console.WriteLine($"Introduce el dato para la posicion{i}");
                respuesta = Console.ReadLine();
                int datosElemento = int.Parse(respuesta);
                datos[i] = datosElemento;
            }

            return datos;
        }
        //static void ProcesaDatos(int[] datos)
        //{
        //    //foreach(int d in datos)
        //    //{
        //    //    Console.WriteLine(d);
        //    //}

        //    for(int i=0; i<4;i++)
        //    {
        //        datos[i] += 10;
        //    }
        //}
    }
}
