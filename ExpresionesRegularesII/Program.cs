﻿using System;
using System.Text.RegularExpressions;

namespace ExpresionesRegularesII
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //string frase = "Mi nombre es Pepito y mi numero es (+526) 555-51-63 o (+1) 555-51-63 y mi codigo postal es 5535";

            //string pattern = @"\+526|\+1";

            //Regex miRegex = new Regex(pattern);

            //MatchCollection elMatch = miRegex.Matches(frase);

            //if (elMatch.Count > 0) Console.WriteLine("Se ha encontrado un numero telefonico de un pais lejano o USA");
            //else Console.WriteLine("No se ha encontrado un num telef");

            //string frase = "Mi web es https://pildorasinformaticas.es";

            //string pattern = "https?://(www.)?pildorasinformaticas.es";

            //Regex miRegex = new Regex(pattern);

            //MatchCollection elMatch = miRegex.Matches(frase);

            //if (elMatch.Count > 0) Console.WriteLine("Se ha encontrado web");
            //else Console.WriteLine("No se ha encontrado");

            string txt = "cursos@pildorasinformaticas.es";

            string re1 = ".*?"; // Non-greedy match on filler
            string re2 = "(@)"; // Any Single Character 1
            string re3 = ".*?"; // Non-greedy match on filler
            string re4 = "(\\.)";   // Any Single Character 2

            Regex r = new Regex(re1 + re2 + re3 + re4, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            Match m = r.Match(txt);
            if (m.Success)
            {
                Console.WriteLine("direccion de EMAIL correcta");
            }
            else
            {
                Console.WriteLine("Email NO correcto");
            }
        }
    }
}