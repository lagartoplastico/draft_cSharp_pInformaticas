﻿using System;

namespace UsoCoches
{
    class Program
    {
        static void Main(string[] args)
        {
            Coche coche1 = new Coche(); /*Crear objetos de tipo coche
                                            dar un estado inicial a nuestro coche*/
            Coche coche2 = new Coche();

            Console.WriteLine(coche1.getInfoCoche());
            Console.WriteLine(coche2.getInfoCoche());

            Coche coche3 = new Coche(4500.25, 1200.35);

            Console.WriteLine(coche3.getInfoCoche());
            coche3.setExtras(true, "cuero");
            Console.WriteLine(coche3.getExtras());
        }
    }

    partial class Coche
    {

        public Coche()
        {
            ruedas = 4;
            largo = 2300.5;
            ancho = 0.800;
            tapiceria = "tela";
        }

        public Coche(double largoCoche, double anchoCoche)
        {
            ruedas = 4;
            largo = largoCoche;
            ancho = anchoCoche;
            tapiceria = "tela";

        }
    }
    partial class Coche
    {
        public string getInfoCoche()
        {
            return "Informacion del coche\nRuedas:  " + ruedas + "\nLargo: " + largo + "\nAncho: " + ancho + "\n";
        }

        public void setExtras(bool climatizador, string tapiceria)
        {
            this.climatizador = climatizador;
            this.tapiceria = tapiceria;
        }
        public string getExtras()
        {
            return "Extras del coche\nTapiceria: " + tapiceria + "\nClimatizacion: " + climatizador;
        }
        private int ruedas;
        private double largo;
        private double ancho;
        private bool climatizador;
        private string tapiceria;

    }

}
